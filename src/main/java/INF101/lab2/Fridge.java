package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    List<FridgeItem> items = new ArrayList<FridgeItem>();

    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() < totalSize()) {
            items.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (items.contains(item)) {
            items.remove(item);
        } else
            throw new NoSuchElementException();
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired = new ArrayList<FridgeItem>();
        for (FridgeItem item : items) {
            if (item.hasExpired()) {
                expired.add(item);
            }
        }
        items.removeAll(expired);
        return expired;
    }

}
